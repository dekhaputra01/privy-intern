import { useReactMediaRecorder } from "react-media-recorder";

const VideoRecord = () => {
  const { status, startRecording, stopRecording, mediaBlobUrl } =
    useReactMediaRecorder({ video: true });
 
  return (
    <div>
      <p>{status}</p>
      <button onClick={startRecording}>Start Recording</button>
      <button onClick={stopRecording}>Stop Recording</button>
      <button onClick={stopRecording}>Pause Recording</button>
      <button onClick={stopRecording}>Resume Recording</button>
      <video hidden src={mediaBlobUrl} />
      <div className="row">
            <a href={mediaBlobUrl} download>Download Video </a>
        </div>

    </div>
  );
};

export default VideoRecord;